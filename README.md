# sxs-manifest-wiper
Tool to remove side by side manifests from Windows PE executables and DLL files.

Usage:
	manifest-wiper.exe file1 [file2] [file3] ...

When unable to remove the manifest it will terminate and return the parameter
index of the file that caused the error.

On success it will return 0.
